﻿using UnityEngine;
using Utils;

/// <summary>
/// Equation based easing demo behaviour.
/// </summary>
public class EaseEquationDemoBehaviour : EaseDemoBehaviour {

	[Tooltip("The equation to use for interpolation")]
	/// <summary>
	/// The equation to use for interpolation.
	/// </summary>
	public Ease.EaseFunctions Equation;

	/// <summary>
	/// Transforming the position.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected override void TransformPosition(float t) {
		Vector3 newPosition = Vector3.zero;
		newPosition.y = Ease.GetEase(Equation, t) * Distance;
		
		transform.position = originalPosition + newPosition;
	}

	/// <summary>
	/// Transforming the scaling.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected override void TransformScaling(float t) {
		float s = Ease.GetEase(Equation, t) + 0.5f;
		transform.localScale = new Vector3(s, s, s);
	}

	/// <summary>
	/// Transforming the rotation.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected override void TransformRotation(float t) {
		transform.localRotation = Ease.GetEase(Equation, t, Quaternion.identity, Quaternion.Euler(0f, 0f, 90f));
	}
}
