﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controller for the ease demo scene.
/// </summary>
public class EaseDemoController : MonoBehaviour {

	[Tooltip("Whether to apply ease to all positions or not.")]
	/// <summary>
	/// Whether to apply ease to all positions or not.
	/// </summary>
	public bool ApplyToPosition = true;
	[Tooltip("Whether to apply ease to all scalings or not.")]
	/// <summary>
	/// Whether to apply ease to all scalings or not.
	/// </summary>
	public bool ApplyToScaling = false;
	[Tooltip("Whether to apply ease to all rotations or not.")]
	/// <summary>
	/// Whether to apply ease to all rotations or not.
	/// </summary>
	public bool ApplyToRotation = false;
	[Tooltip("Wether to loop all or not.")]
	/// <summary>
	/// Wether to loop all or not.
	/// </summary>
	public bool Loop = true;

	void Start () {
		// Apply settings to static reference of the demo behaviours
		EaseDemoBehaviour.ApplyToPosition = ApplyToPosition;
		EaseDemoBehaviour.ApplyToScaling = ApplyToScaling;
		EaseDemoBehaviour.ApplyToRotation = ApplyToRotation;
		EaseDemoBehaviour.Loop = Loop;
	}
	
	void Update () {
		// Apply settings to static reference of the demo behaviours
		EaseDemoBehaviour.ApplyToPosition = ApplyToPosition;
		EaseDemoBehaviour.ApplyToScaling = ApplyToScaling;
		EaseDemoBehaviour.ApplyToRotation = ApplyToRotation;
		EaseDemoBehaviour.Loop = Loop;
	}
}
