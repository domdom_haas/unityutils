using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Floats.
	/// </summary>
	public class SinusFloat : Effect {

		[Header("Positioning")]
		[Tooltip("The direction to float in.")]
		/// <summary>
		/// The direction to float in.
		/// </summary>
		public Vector3 Direction;
		[Tooltip("The amount to float.")]
		/// <summary>
		/// The amount to float.
		/// </summary>
		public float Amount;
		[Header("Effect parameters")]
		[Tooltip("How many times to float.")]
		/// <summary>
		/// How many times to float.
		/// </summary>
		public int Times;

		/// <summary>
		/// The original position.
		/// </summary>
		private Vector3 originalPosition;
		/// <summary>
		/// The normalized direction.
		/// </summary>
		private Vector3 normalizedDirection;

		protected override void InitEffect() {
			// Get original position and normalized direction
			originalPosition = transform.localPosition;
			normalizedDirection = Direction.normalized;
		}
		
		protected override void UpdateEffect() {
			// Float
			transform.localPosition = originalPosition + normalizedDirection * Amount *  Mathf.Sin(currentTime / (Duration / Times) * Mathf.PI);
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}
