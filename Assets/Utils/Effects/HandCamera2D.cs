using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Hand camera 2d effect.
	/// </summary>
	public class HandCamera2D : Effect {
		
		[Header("Effect parameters")]
		[Tooltip("The amount of the effect. Result is also dependent on the given duration (this effect is infinite).")]
		/// <summary>
		/// The amount of the effect. Result is also dependent on the given duration (this effect is infinite).
		/// </summary>
		public float Amount = 1f;
		[Tooltip("The ease function.")]
		/// <summary>
		/// The ease function.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;

		/// <summary>
		/// The local duration for hand shaking.
		/// </summary>
		private float localDuration;
		/// <summary>
		/// The local current time for hand shaking.
		/// </summary>
		private float localCurrentTime;
		/// <summary>
		/// The original position.
		/// </summary>
		private Vector2 originalPosition;
		/// <summary>
		/// The start position.
		/// </summary>
		private Vector2 startPosition;
		/// <summary>
		/// The target position.
		/// </summary>
		private Vector2 targetPosition;
		
		protected override void InitEffect() {
			infinite = true;

			localDuration = Random.Range(Duration / 2f, Duration);
			localCurrentTime = currentTime;

			// Get and calculate positions
			originalPosition = transform.localPosition;
			startPosition = originalPosition;
			targetPosition = originalPosition + Random.insideUnitCircle * Random.Range(0f, (Amount / 2f));
		}
		
		protected override void UpdateEffect() {
			// Increase local current time
			localCurrentTime += Time.deltaTime;

			// Hand shaking
			transform.localPosition = Ease.GetEase(EaseFunction, localCurrentTime / localDuration, startPosition, targetPosition);
			if (localCurrentTime / localDuration >= 1f) {
				localCurrentTime -= localDuration;
				localDuration = Random.Range(Duration / 2f, localDuration);
				startPosition = targetPosition;
				targetPosition = originalPosition + Random.insideUnitCircle * Random.Range(0f, (Amount / 2f));
			}

			// Breathing
			transform.localPosition = (Vector2) transform.localPosition + Vector2.up * (Amount / 8f) *  Mathf.Sin(currentTime / Duration);
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}
