using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Scales.
	/// </summary>
	public class Scale : Effect {
		
		[Header("Function parameters")]
		[Tooltip("Determies whether to use the curve or the ease function.")]
		/// <summary>
		/// Determies whether to use the curve or the ease function.
		/// </summary>
		public bool UseCurve = false;
		[Tooltip("The curve.")]
		/// <summary>
		/// The curve.
		/// </summary>
		public AnimationCurve Curve;
		[Tooltip("The ease function.")]
		/// <summary>
		/// The ease function.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;
		
		[Header("Effect parameters")]
		[Tooltip("Whether to reset to the original position at the end.")]
		/// <summary>
		/// Whether to reset to the original position at the end.
		/// </summary>
		public bool ResetAtTheEnd = false;
		[Tooltip("The axis influence.")]
		/// <summary>
		/// The axis influence.
		/// </summary>
		public Vector3 Influence;
		[Tooltip("The amount to scale by.")]
		/// <summary>
		/// The amount to scale by.
		/// </summary>
		public float Amount;

		/// <summary>
		/// The original scale.
		/// </summary>
		private Vector3 originalScale;
		/// <summary>
		/// The target scale.
		/// </summary>
		private Vector3 targetScale;

		protected override void InitEffect() {
			// Get original scale and calculate target scale
			originalScale = transform.localScale;
			targetScale = originalScale + Influence * Amount;
		}
		
		protected override void UpdateEffect() {
			// Update scale according to settings
			if (UseCurve) transform.localScale = Ease.GetEase(Curve, currentTime / Duration, originalScale, targetScale);
			else transform.localScale = Ease.GetEase(EaseFunction, currentTime / Duration, originalScale, targetScale);
		}
		
		protected override void CleanUpEffect() {
			// Reset scale if necessairy
			if (ResetAtTheEnd) transform.localScale = originalScale;
		}
	}
}
