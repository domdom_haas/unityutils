using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Flickers the alpha value of all materials.
	/// </summary>
	public class Flicker : Effect {

		[Header("Effect parameters")]
		[Tooltip("How many times to flicker.")]
		/// <summary>
		/// How many times to flicker.
		/// </summary>
		public int Times;
		[Tooltip("Determies wheter to fade out at the end or to return to normal.")]
		/// <summary>
		/// Determies wheter to fade out at the end or to return to normal.
		/// </summary>
		public bool FadeOut;

		/// <summary>
		/// Reference to the renderer.
		/// </summary>
		private Renderer r;
		/// <summary>
		/// The amount it will flicker (used for fadeout).
		/// </summary>
		private float amount;
		/// <summary>
		/// All colors of the materials.
		/// </summary>
		private Color[] colors;

		protected override void InitEffect() {
			// Get renderer reference
			r = GetComponent<Renderer>();

			// Calculate amount
			amount = Times;
			if (FadeOut) amount += 0.5f;

			// Get all colors
			colors = new Color[r.materials.Length];
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i] = r.materials[i].color;
			}
		}
		
		protected override void UpdateEffect() {
			// Calculate alpha value
			float a = 1f - Mathf.Abs(Mathf.Sin(currentTime / Duration * amount * Mathf.PI)); // TODO: Use start alpha instead of 1f!

			// Apply alpha value to all colors
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i].a = a;
				r.materials[i].color = colors[i];
			}
		}
		
		protected override void CleanUpEffect() {
			// Apply final alpha value to all colors
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i].a = (FadeOut) ? 0f : 1f;
				r.materials[i].color = colors[i];
			}
		}
	}
}