﻿using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Effects base class.
	/// </summary>
	public abstract class Effect : MonoBehaviour {

		[Header("Timing")]
		[Tooltip("Determines wheter to automatically start the effect on creation.")]
		/// <summary>
		/// Determines wheter to automatically start the effect on creation.
		/// </summary>
		public bool AutoStart = true;
		[Tooltip("A delay to start after (in seconds).")]
		/// <summary>
		/// A delay to start after (in seconds).
		/// </summary>
		public float Delay = 0f;
		[Tooltip("The duration of the effect.")]
		/// <summary>
		/// The duration of the effect.
		/// </summary>
		public float Duration = 1f;

		[HideInInspector]
		/// <summary>
		/// Determines whether to remove the effect script after execution.
		/// Won't destroy the game object.
		/// </summary>
		public bool DeleteSelfAfterExecution = false;

		/// <summary>
		/// Indicates whether the effect is infinite. Override this in your own effects.
		/// </summary>
		protected bool infinite = false;
		/// <summary>
		/// The current time (in seconds).
		/// </summary>
		protected float currentTime;
		/// <summary>
		/// The current delay (in seconds).
		/// </summary>
		private float currentDelay;

		/// <summary>
		/// Indicates whether the effect has started or not.
		/// </summary>
		protected bool hasStarted = false;
		/// <summary>
		/// Gets a value indicating whether the effect has started.
		/// </summary>
		/// <value><c>true</c> if the effect has started; otherwise, <c>false</c>.</value>
		public bool HasStarted {
			get { return hasStarted; }
		}

		/// <summary>
		/// Indicates whether the effect is playing or not.
		/// </summary>
		protected bool isPlaying = false;
		/// <summary>
		/// Gets a value indicating whether the effect is playing.
		/// </summary>
		/// <value><c>true</c> if the effect is playing; otherwise, <c>false</c>.</value>
		public bool IsPlaying {
			get { return isPlaying; }
		}

		void Start() {
			// Start the effect automatically if necessairy
			if (AutoStart) StartEffect();
		}

		void Update() {
			// I the effect has started
			if (hasStarted) {
				// Handle delay
				if (currentDelay > 0f) currentDelay -= Time.deltaTime;
				else {
					// Update the effect
					isPlaying = true;
					UpdateEffect();
					currentTime += Time.deltaTime;
				}
			}

			// Handle effect ending
			if (!infinite && currentTime >= Duration) EndEffect();
		}

		/// <summary>
		/// Starts the effect.
		/// </summary>
		public void StartEffect() {
			// Handle restart
			if (hasStarted) {
				if (infinite || DeleteSelfAfterExecution) return;
				else EndEffect();
			}

			// Start effect
			hasStarted = true;
			currentDelay = Delay;
			InitEffect();
		}

		/// <summary>
		/// Ends the effect.
		/// </summary>
		public void EndEffect() {
			// End effect
			CleanUpEffect();
			if (DeleteSelfAfterExecution) Destroy(this);
		}

		/// <summary>
		/// Method to initialise the effect. Override this in your own effects.
		/// </summary>
		protected virtual void InitEffect() { }

		/// <summary>
		/// Method to update the effect. Override this in your own effects.
		/// </summary>
		protected virtual void UpdateEffect() { }

		/// <summary>
		/// Method to clean up the effect. Override this in your own effects.
		/// </summary>
		protected virtual void CleanUpEffect() { }
	}
}
