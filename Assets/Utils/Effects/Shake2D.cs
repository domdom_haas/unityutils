using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Shakes (2d).
	/// </summary>
	public class Shake2D : Effect {

		[Header("Effect parameters")]
		[Tooltip("The amount to shake by.")]
		/// <summary>
		/// The amount to shake by.
		/// </summary>
		public float Amount;

		/// <summary>
		/// The original position.
		/// </summary>
		private Vector2 originalPosition;

		protected override void InitEffect() {
			// Get original position
			originalPosition = transform.localPosition;
		}
		
		protected override void UpdateEffect() {
			// Get random position every update
			transform.localPosition = originalPosition + Random.insideUnitCircle * Amount;
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}
