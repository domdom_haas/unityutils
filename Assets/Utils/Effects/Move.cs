using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Moves.
	/// </summary>
	public class Move : Effect {
		
		[Header("Function parameters")]
		[Tooltip("Determies whether to use the curve or the ease function.")]
		/// <summary>
		/// Determies whether to use the curve or the ease function.
		/// </summary>
		public bool UseCurve = false;
		[Tooltip("The curve.")]
		/// <summary>
		/// The curve.
		/// </summary>
		public AnimationCurve Curve;
		[Tooltip("The ease function.")]
		/// <summary>
		/// The ease function.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;

		[Header("Effect parameters")]
		[Tooltip("Whether to reset to the original position at the end.")]
		/// <summary>
		/// Whether to reset to the original position at the end.
		/// </summary>
		public bool ResetAtTheEnd = false;
		[Tooltip("The direction.")]
		/// <summary>
		/// The direction.
		/// </summary>
		public Vector3 Direction;
		[Tooltip("The amount to move by.")]
		/// <summary>
		/// The amount to move by.
		/// </summary>
		public float Amount;

		/// <summary>
		/// The original position.
		/// </summary>
		private Vector3 originalPosition;
		/// <summary>
		/// The target position.
		/// </summary>
		private Vector3 targetPosition;
		
		protected override void InitEffect() {
			// Get original position and calculate target position
			originalPosition = transform.localPosition;
			targetPosition = originalPosition + Direction * Amount;
		}
		
		protected override void UpdateEffect() {
			// Update position according to settings
			if (UseCurve) transform.localPosition = Ease.GetEase(Curve, currentTime / Duration, originalPosition, targetPosition);
			else transform.localPosition = Ease.GetEase(EaseFunction, currentTime / Duration, originalPosition, targetPosition);
		}
		
		protected override void CleanUpEffect() {
			// Reset position if necessairy
			if (ResetAtTheEnd) transform.localPosition = originalPosition;
		}
	}
}
