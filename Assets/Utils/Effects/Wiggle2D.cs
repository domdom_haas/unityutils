using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Wiggles (2d).
	/// </summary>
	public class Wiggle2D : Effect {

		[Header("Effect parameters")]
		[Tooltip("The amount to wiggle.")]
		/// <summary>
		/// The amount to wiggle.
		/// </summary>
		public float Amount;
		[Tooltip("How many times to wiggle.")]
		/// <summary>
		/// How many times to wiggle.
		/// </summary>
		public int Times;
		[Tooltip("The ease function for interpolation.")]
		/// <summary>
		/// The ease function for interpolation.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;

		/// <summary>
		/// The original position.
		/// </summary>
		private Vector2 originalPosition;
		/// <summary>
		/// The start position.
		/// </summary>
		private Vector2 startPosition;
		/// <summary>
		/// The target position.
		/// </summary>
		private Vector2 targetPosition;
		/// <summary>
		/// The iterations. Used to end on the original position.
		/// </summary>
		private float iterations;
		/// <summary>
		/// The current iteration. Used to end on the original position.
		/// </summary>
		private int currentIteration;
		
		protected override void InitEffect() {
			// Get inital positions
			originalPosition = transform.localPosition;
			startPosition = originalPosition;
			targetPosition = originalPosition + Random.insideUnitCircle * (Amount / 2f);

			// Get iterations
			iterations = Duration / (Times + 1);
			currentIteration = 0;
		}
		
		protected override void UpdateEffect() {
			// Wiggle like a motherfucker
			float t = (currentTime - currentIteration * iterations) / iterations;
			
			transform.localPosition = Ease.GetEase(EaseFunction, t, startPosition, targetPosition);
			
			if (t >= 1f) {
				currentIteration++;
				startPosition = targetPosition;
				if (currentIteration < Times) targetPosition = originalPosition + Random.insideUnitCircle * (Amount / 2f);
				else targetPosition = originalPosition;
			}
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}
