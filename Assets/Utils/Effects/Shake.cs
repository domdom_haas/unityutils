using UnityEngine;

namespace Utils.Effects {

	/// <summary>
	/// Shakes.
	/// </summary>
	public class Shake : Effect {

		[Header("Effect parameters")]
		[Tooltip("The amount to shake by.")]
		/// <summary>
		/// The amount to shake by.
		/// </summary>
		public float Amount;

		/// <summary>
		/// The original position.
		/// </summary>
		private Vector3 originalPosition;

		protected override void InitEffect() {
			// Get original position
			originalPosition = transform.localPosition;
		}
		
		protected override void UpdateEffect() {
			// Get random position every update
			transform.localPosition = originalPosition + Random.insideUnitSphere * Amount;
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}
