﻿using UnityEngine;

namespace Utils.Helpers {

	/// <summary>
	/// Helper for level loading.
	/// </summary>
	public abstract class LevelHelper {

		/// <summary>
		/// Reloads the current level.
		/// </summary>
		public static void ReloadCurrentLevel() {
			Application.LoadLevel(Application.loadedLevel);
		}
	
		/// <summary>
		/// Loads the next level.
		/// </summary>
		/// <param name="index">Index of the level. -1 for current level.</param>
		/// <param name="cycle">Determines what happens if it's the last level. If set to <c>true</c> loads the first level, if set to <c>false</c> loads the last level.</param>
		public static void LoadNextLevel(int index = -1, bool cycle = true) {
			Application.LoadLevel(NextLevel(index, cycle));
		}

		/// <summary>
		/// Loads the previous level.
		/// </summary>
		/// <param name="index">Index of the level. -1 for current level.</param>
		/// <param name="cycle">Determines what happens if it's the first level. If set to <c>true</c> loads the last level, if set to <c>false</c> loads the first level.</param>
		public static void LoadPreviousLevel(int index = -1, bool cycle = true) {
			Application.LoadLevel(PreviousLevel(index, cycle));
		}

		/// <summary>
		/// Loads the first level or a level with a given offset to the first level.
		/// </summary>
		/// <param name="offset">Offset in positive number.</param>
		public static void LoadFirstLevel(int offset = 0) {
			if (offset >= Application.levelCount) {
				Debug.LogWarning("Offset to big. Loading last level!");
				offset = Application.levelCount - 1;
			}
			if (offset < 0) {
				Debug.LogWarning("Offset smaller than zero. Loading first level!");
				offset = 0;
			}
			Application.LoadLevel(0 + offset);
		}

		/// <summary>
		/// Loads the last level or a level with a given offset to the last level.
		/// </summary>
		/// <param name="offset">Offset in positive number.</param>
		public static void LoadLastLevel(int offset = 0) {
			if (offset >= Application.levelCount) {
				Debug.LogWarning("Offset to big. Loading first level!");
				offset = Application.levelCount - 1;
			}
			if (offset < 0) {
				Debug.LogWarning("Offset smaller than zero. Loading last level!");
				offset = 0;
			}
			Application.LoadLevel(Application.levelCount - 1 - offset);
		}

		/// <summary>
		/// Gets the index of the next level.
		/// </summary>
		/// <returns>The next levels index.</returns>
		/// <param name="index">Index of the level. -1 for current level.</param>
		/// <param name="cycle">Determines what happens if it's the last level. If set to <c>true</c> returns the first level, if set to <c>false</c> returns the last level.</param>
		public static int NextLevel(int index = -1, bool cycle = true) {
			int next = (index >= 0) ? index : Application.loadedLevel;
			next++;
			if (next >= Application.levelCount) {
				if (cycle) next -= Application.levelCount;
				else next = Application.levelCount - 1;
			}
			return next;
		}

		/// <summary>
		/// Gets the index of the previous level.
		/// </summary>
		/// <returns>The previous levels index.</returns>
		/// <param name="index">Index of the level. -1 for current level.</param>
		/// <param name="cycle">Determines what happens if it's the first level. If set to <c>true</c> returns the last level, if set to <c>false</c> returns the first level.</param>
		public static int PreviousLevel(int index = -1, bool cycle = true) {
			int previous = (index >= 0) ? index : Application.loadedLevel;
			previous--;
			if (previous < 0) {
				if (cycle) previous += Application.levelCount;
				else previous = 0;
			}
			return previous;
		}

		/// <summary>
		/// Checks if it is the first level.
		/// </summary>
		/// <returns><c>true</c> if is the first level; otherwise, <c>false</c>.</returns>
		/// <param name="index">Index of the level. -1 for current level.</param>
		public static bool IsFirstLevel(int index = -1) {
			if (index >= 0) return index == 0;
			else return Application.loadedLevel == 0;
		}
	
		/// <summary>
		/// Checks if it is the last level.
		/// </summary>
		/// <returns><c>true</c> if is the last level; otherwise, <c>false</c>.</returns>
		/// <param name="index">Index of the level. -1 for current level.</param>
		public static bool IsLastLevel(int index = -1) {
			if (index >= 0) return index == Application.levelCount - 1;
			else return Application.loadedLevel == Application.levelCount - 1;
		}
	}
}
