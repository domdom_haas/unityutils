﻿using UnityEngine;

namespace Utils.Helpers {

	/// <summary>
	/// Audio helper methods.
	/// </summary>
	public abstract class AudioHelper {

		/// <summary>
		/// Randomizes the pitch of a given audio source.
		/// </summary>
		/// <param name="audioSource">Audio source.</param>
		/// <param name="subtract">Subtract.</param>
		/// <param name="add">Add.</param>
		public static void RandomizePitch(AudioSource audioSource, float subtract = -0.2f, float add = 0.2f) {
			audioSource.pitch += Random.Range(subtract, add);
		}

		/// <summary>
		/// Randimizes the volume of a given audio source.
		/// </summary>
		/// <param name="audioSource">Audio source.</param>
		/// <param name="subtract">Subtract.</param>
		/// <param name="add">Add.</param>
		public static void RandimizeVolume(AudioSource audioSource, float subtract = -0.2f, float add = 0.2f) {
			audioSource.volume += Random.Range(subtract, add);
		}
	}
}
