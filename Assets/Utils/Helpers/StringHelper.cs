﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.Helpers {
	
	/// <summary>
	/// Helper methods for strings.
	/// </summary>
	public abstract class StringHelper {

		/// <summary>
		/// Splits a given text pagewise for a text generator.
		/// </summary>
		/// <returns>The text split into pages.</returns>
		/// <param name="text">The text to split.</param>
		/// <param name="settings">The text generator settings.</param>
		public static List<string> SplitPagewise(string text, Text uiText) {
			List<string> pages = new List<string>();
			TextGenerationSettings settings = uiText.GetGenerationSettings(uiText.rectTransform.rect.size);
			TextGenerator generator = uiText.cachedTextGenerator;

			// While there is text
			while (text.Length > 0) {
				// Populate generator to get size
				generator.Populate (text, settings);

				// Get displayable string
				string display = text.Substring(0, generator.characterCountVisible);
				if (string.IsNullOrEmpty(display)) break; // Prevent infinite loop

				// Make word wrap correct because unity cannot word wrap!
				if (display.Length > 0 && text.Length > display.Length) {
					if (display[display.Length - 1] != ' ' && text[display.Length] != ' ') {
						string[] tempDisplayArray = display.Split(' ');
						display = display.TrimEnd(tempDisplayArray[tempDisplayArray.Length - 1].ToCharArray());
					}
				}

				pages.Add(display.Trim());				// Add display string to list of pages
				text = text.Remove(0, display.Length);	// Remove handled text segment / page
				generator.Invalidate();					// Invalidate generator for next text segment
			}

			return pages;
		}
	}
}
