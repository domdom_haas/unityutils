﻿using UnityEngine;

namespace Utils.Behaviours {

	/// <summary>
	/// Destroys a gameobject with a given delay.
	/// </summary>
	public class DestroyWithDelay : MonoBehaviour {

		[Tooltip("The delay in seconds with which the object will be destroyed.")]
		/// <summary>
		/// The delay after instantiation.
		/// </summary>
		public float Delay = 1f;

		/// <summary>
		/// The current time until destruction.
		/// </summary>
		private float currentTime;

		void Start () {
			currentTime = Time.deltaTime;
		}

		void Update() {
			currentTime += Time.deltaTime;
			if (currentTime >= Delay) Destroy(gameObject);
		}
	}
}
