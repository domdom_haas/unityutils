﻿using UnityEngine;
using System.Collections.Generic;

namespace Utils.Behaviours {

	/// <summary>
	/// Destroy game objects based on a trigger.
	/// </summary>
	public class DestroyGameObjectTrigger : MonoBehaviour {

		[Tooltip("The game objects that trigger.")]
		/// <summary>
		/// The game objects that trigger.
		/// </summary>
		public List<GameObject> gameObjectsThatTrigger;
		[Tooltip("The game objects to destroy.")]
		/// <summary>
		/// The game objects to destroy.
		/// </summary>
		public List<GameObject> gameObjectsToDestroy;
		[Tooltip("Determines wheter the script destroys itself.")]
		/// <summary>
		/// Determines wheter the script destroys itself.
		/// </summary>
		public bool DestroySelf = true;

		void OnTriggerEnter(Collider collider) {
			// If collider is one of the objects that can trigger
			if (gameObjectsThatTrigger.Contains(collider.gameObject)) {
				// Destroy game objects
				foreach(GameObject go in gameObjectsToDestroy) {
					if (go != null) Destroy(go);
				}
	
				// Destroy yourself or clear the list with the now inexistent game objects
				if (DestroySelf) Destroy(gameObject);
				else gameObjectsToDestroy.Clear();
			}
		}
	}
}
