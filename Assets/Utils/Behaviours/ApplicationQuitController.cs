﻿using UnityEngine;

namespace Utils.Behaviours {

	/// <summary>
	/// Enables you to let the game be quit by the press of a key or a button.
	/// If a button name is given the key will be ignored.
	/// </summary>
	public class ApplicationQuitController : MonoBehaviour {

		[Tooltip("The key that quits the application.\nIf a button name is given the key will be ignored.")]
		/// <summary>
		/// The key.
		/// </summary>
		public KeyCode Key  = KeyCode.Escape;
		[Tooltip("The name of the button that quits the application.\nIf a button name is given the key will be ignored.")]
		/// <summary>
		/// The name of the button.
		/// </summary>
		public string ButtonName;
		
		/// <summary>
		/// Poll input every update.
		/// </summary>
		void Update () {
			if ((string.IsNullOrEmpty(ButtonName)) ? UnityEngine.Input.GetKeyDown(Key) : UnityEngine.Input.GetButtonDown(ButtonName))
				Application.Quit();
		}
	}
}
